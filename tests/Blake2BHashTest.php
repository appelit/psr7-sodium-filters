<?php namespace Tests;

use APPelit\Psr7\SodiumFilters\Blake2BHash;
use Codeception\Test\Unit;
use function GuzzleHttp\Psr7\stream_for;

class Blake2BHashTest extends Unit
{

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests

    /**
     * @throws \Exception
     */
    public function testGeneratesCorrectHashWithDefaults()
    {
        $testString = \random_bytes(1024);

        $expectedHash = \sodium_crypto_generichash($testString);

        $stream = stream_for($testString);

        $hashingStream = new Blake2BHash($stream);

        $this->assertEquals($expectedHash, $hashingStream->getContents());
    }

    /**
     * @throws \Exception
     */
    public function testGeneratesCorrectHashWithKey()
    {
        $testString = \random_bytes(1024);

        $key = \sodium_crypto_generichash_keygen();

        $expectedHash = \sodium_crypto_generichash($testString, $key);

        $stream = stream_for($testString);

        $hashingStream = new Blake2BHash($stream, SODIUM_CRYPTO_GENERICHASH_BYTES, $key);

        $this->assertEquals($expectedHash, $hashingStream->getContents());
    }

    /**
     * @throws \Exception
     */
    public function testGeneratesCorrectHashWithSize()
    {
        $testString = \random_bytes(1024);

        $hashSize = random_int(SODIUM_CRYPTO_GENERICHASH_BYTES_MIN, SODIUM_CRYPTO_GENERICHASH_BYTES_MAX);

        $expectedHash = \sodium_crypto_generichash($testString, '', $hashSize);

        $stream = stream_for($testString);

        $hashingStream = new Blake2BHash($stream, $hashSize, '');

        $this->assertEquals($expectedHash, $hashingStream->getContents());
    }

    /**
     * @throws \Exception
     */
    public function testGeneratesCorrectHashWithKeyAndSize()
    {
        $testString = \random_bytes(1024);

        $key = \sodium_crypto_generichash_keygen();

        $hashSize = random_int(SODIUM_CRYPTO_GENERICHASH_BYTES_MIN, SODIUM_CRYPTO_GENERICHASH_BYTES_MAX);

        $expectedHash = \sodium_crypto_generichash($testString, $key, $hashSize);

        $stream = stream_for($testString);

        $hashingStream = new Blake2BHash($stream, $hashSize, $key);

        $this->assertEquals($expectedHash, $hashingStream->getContents());
    }

    /**
     * @throws \Exception
     */
    public function testGeneratesCorrectHashFromSmallFileWithDefaults()
    {
        $expectedHash = \sodium_crypto_generichash(file_get_contents(__DIR__ . '/_support/testdata-small.bin'));

        $stream = stream_for(fopen(__DIR__ . '/_support/testdata-small.bin', 'rb'));

        $hashingStream = new Blake2BHash($stream);

        $this->assertEquals($expectedHash, $hashingStream->getContents());
    }

    /**
     * @throws \Exception
     */
    public function testGeneratesCorrectHashFromSmallFileWithKey()
    {
        $key = \sodium_crypto_generichash_keygen();

        $expectedHash = \sodium_crypto_generichash(file_get_contents(__DIR__ . '/_support/testdata-small.bin'), $key);

        $stream = stream_for(fopen(__DIR__ . '/_support/testdata-small.bin', 'rb'));

        $hashingStream = new Blake2BHash($stream, SODIUM_CRYPTO_GENERICHASH_BYTES, $key);

        $this->assertEquals($expectedHash, $hashingStream->getContents());
    }

    /**
     * @throws \Exception
     */
    public function testGeneratesCorrectHashFromSmallFileWithSize()
    {
        $hashSize = random_int(SODIUM_CRYPTO_GENERICHASH_BYTES_MIN, SODIUM_CRYPTO_GENERICHASH_BYTES_MAX);

        $expectedHash = \sodium_crypto_generichash(file_get_contents(__DIR__ . '/_support/testdata-small.bin'), '', $hashSize);

        $stream = stream_for(fopen(__DIR__ . '/_support/testdata-small.bin', 'rb'));

        $hashingStream = new Blake2BHash($stream, $hashSize, '');

        $this->assertEquals($expectedHash, $hashingStream->getContents());
    }

    /**
     * @throws \Exception
     */
    public function testGeneratesCorrectHashFromSmallFileWithKeyAndSize()
    {
        $key = \sodium_crypto_generichash_keygen();

        $hashSize = random_int(SODIUM_CRYPTO_GENERICHASH_BYTES_MIN, SODIUM_CRYPTO_GENERICHASH_BYTES_MAX);

        $expectedHash = \sodium_crypto_generichash(file_get_contents(__DIR__ . '/_support/testdata-small.bin'), $key, $hashSize);

        $stream = stream_for(fopen(__DIR__ . '/_support/testdata-small.bin', 'rb'));

        $hashingStream = new Blake2BHash($stream, $hashSize, $key);

        $this->assertEquals($expectedHash, $hashingStream->getContents());
    }

    /**
     * @throws \Exception
     */
    public function testGeneratesCorrectHashFromMediumFileWithDefaults()
    {
        $expectedHash = \sodium_crypto_generichash(file_get_contents(__DIR__ . '/_support/testdata-medium.bin'));

        $stream = stream_for(fopen(__DIR__ . '/_support/testdata-medium.bin', 'rb'));

        $hashingStream = new Blake2BHash($stream);

        $this->assertEquals($expectedHash, $hashingStream->getContents());
    }

    /**
     * @throws \Exception
     */
    public function testGeneratesCorrectHashFromMediumFileWithKey()
    {
        $key = \sodium_crypto_generichash_keygen();

        $expectedHash = \sodium_crypto_generichash(file_get_contents(__DIR__ . '/_support/testdata-medium.bin'), $key);

        $stream = stream_for(fopen(__DIR__ . '/_support/testdata-medium.bin', 'rb'));

        $hashingStream = new Blake2BHash($stream, SODIUM_CRYPTO_GENERICHASH_BYTES, $key);

        $this->assertEquals($expectedHash, $hashingStream->getContents());
    }

    /**
     * @throws \Exception
     */
    public function testGeneratesCorrectHashFromMediumFileWithSize()
    {
        $hashSize = random_int(SODIUM_CRYPTO_GENERICHASH_BYTES_MIN, SODIUM_CRYPTO_GENERICHASH_BYTES_MAX);

        $expectedHash = \sodium_crypto_generichash(file_get_contents(__DIR__ . '/_support/testdata-medium.bin'), '', $hashSize);

        $stream = stream_for(fopen(__DIR__ . '/_support/testdata-medium.bin', 'rb'));

        $hashingStream = new Blake2BHash($stream, $hashSize, '');

        $this->assertEquals($expectedHash, $hashingStream->getContents());
    }

    /**
     * @throws \Exception
     */
    public function testGeneratesCorrectHashFromMediumFileWithKeyAndSize()
    {
        $key = \sodium_crypto_generichash_keygen();

        $hashSize = random_int(SODIUM_CRYPTO_GENERICHASH_BYTES_MIN, SODIUM_CRYPTO_GENERICHASH_BYTES_MAX);

        $expectedHash = \sodium_crypto_generichash(file_get_contents(__DIR__ . '/_support/testdata-medium.bin'), $key, $hashSize);

        $stream = stream_for(fopen(__DIR__ . '/_support/testdata-medium.bin', 'rb'));

        $hashingStream = new Blake2BHash($stream, $hashSize, $key);

        $this->assertEquals($expectedHash, $hashingStream->getContents());
    }

    /**
     * @throws \Exception
     */
    public function testGeneratesCorrectHashFromLargeFileWithDefaults()
    {
        $expectedHash = \sodium_crypto_generichash(file_get_contents(__DIR__ . '/_support/testdata-large.bin'));

        $stream = stream_for(fopen(__DIR__ . '/_support/testdata-large.bin', 'rb'));

        $hashingStream = new Blake2BHash($stream);

        $this->assertEquals($expectedHash, $hashingStream->getContents());
    }

    /**
     * @throws \Exception
     */
    public function testGeneratesCorrectHashFromLargeFileWithKey()
    {
        $key = \sodium_crypto_generichash_keygen();

        $expectedHash = \sodium_crypto_generichash(file_get_contents(__DIR__ . '/_support/testdata-large.bin'), $key);

        $stream = stream_for(fopen(__DIR__ . '/_support/testdata-large.bin', 'rb'));

        $hashingStream = new Blake2BHash($stream, SODIUM_CRYPTO_GENERICHASH_BYTES, $key);

        $this->assertEquals($expectedHash, $hashingStream->getContents());
    }

    /**
     * @throws \Exception
     */
    public function testGeneratesCorrectHashFromLargeFileWithSize()
    {
        $hashSize = random_int(SODIUM_CRYPTO_GENERICHASH_BYTES_MIN, SODIUM_CRYPTO_GENERICHASH_BYTES_MAX);

        $expectedHash = \sodium_crypto_generichash(file_get_contents(__DIR__ . '/_support/testdata-large.bin'), '', $hashSize);

        $stream = stream_for(fopen(__DIR__ . '/_support/testdata-large.bin', 'rb'));

        $hashingStream = new Blake2BHash($stream, $hashSize, '');

        $this->assertEquals($expectedHash, $hashingStream->getContents());
    }

    /**
     * @throws \Exception
     */
    public function testGeneratesCorrectHashFromLargeFileWithKeyAndSize()
    {
        $key = \sodium_crypto_generichash_keygen();

        $hashSize = random_int(SODIUM_CRYPTO_GENERICHASH_BYTES_MIN, SODIUM_CRYPTO_GENERICHASH_BYTES_MAX);

        $expectedHash = \sodium_crypto_generichash(file_get_contents(__DIR__ . '/_support/testdata-large.bin'), $key, $hashSize);

        $stream = stream_for(fopen(__DIR__ . '/_support/testdata-large.bin', 'rb'));

        $hashingStream = new Blake2BHash($stream, $hashSize, $key);

        $this->assertEquals($expectedHash, $hashingStream->getContents());
    }
}
