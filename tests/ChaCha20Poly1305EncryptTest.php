<?php namespace Tests;

use APPelit\Psr7\SodiumFilters\ChaCha20Poly1305Encrypt;
use Codeception\Test\Unit;
use function GuzzleHttp\Psr7\stream_for;

class ChaCha20Poly1305EncryptTest extends Unit
{

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests

    /**
     * @throws \Exception
     */
    public function testCorrectlyEncryptsWithDefaults()
    {
        $testString = \random_bytes(1024);

        $key = \sodium_crypto_aead_chacha20poly1305_keygen();

        $stream = stream_for($testString);

        $encryptingStream = new Chacha20Poly1305Encrypt($stream, $key);

        $nonce = $encryptingStream->getNonce();

        $expectedValue = \sodium_crypto_aead_chacha20poly1305_encrypt(
            $testString,
            str_repeat(chr(0), strlen($nonce)),
            $nonce,
            $key
        );

        $this->assertEquals($nonce . $expectedValue, $encryptingStream->getContents());
    }

    /**
     * @throws \Exception
     */
    public function testCorrectlyEncryptsWithAdditionalData()
    {
        $testString = \random_bytes(1024);

        $additionalData = \random_bytes(1024);

        $key = \sodium_crypto_aead_chacha20poly1305_keygen();

        $stream = stream_for($testString);

        $encryptingStream = new Chacha20Poly1305Encrypt($stream, $key, $additionalData);

        $nonce = $encryptingStream->getNonce();

        $expectedValue = \sodium_crypto_aead_chacha20poly1305_encrypt(
            $testString,
            str_repeat(chr(0), strlen($nonce)) . $additionalData,
            $nonce,
            $key
        );

        $this->assertEquals($nonce . $expectedValue, $encryptingStream->getContents());
    }

    /**
     * @throws \Exception
     */
    public function testCorrectlyEncryptsSmallFileWithDefaults()
    {
        $key = \sodium_crypto_aead_chacha20poly1305_keygen();

        $stream = stream_for(fopen(__DIR__ . '/_support/testdata-small.bin', 'rb'));

        $encryptingStream = new Chacha20Poly1305Encrypt($stream, $key);

        $nonce = $encryptingStream->getNonce();

        $expectedValue = $this->generateExpectedValue(
            file_get_contents(__DIR__ . '/_support/testdata-small.bin'),
            $key,
            $nonce
        );

        $this->assertEquals($nonce . $expectedValue, $encryptingStream->getContents());
    }

    /**
     * @throws \Exception
     */
    public function testCorrectlyEncryptsSmallFileWithAdditionalData()
    {
        $additionalData = \random_bytes(1024);

        $key = \sodium_crypto_aead_chacha20poly1305_keygen();

        $stream = stream_for(fopen(__DIR__ . '/_support/testdata-small.bin', 'rb'));

        $encryptingStream = new Chacha20Poly1305Encrypt($stream, $key, $additionalData);

        $nonce = $encryptingStream->getNonce();

        $expectedValue = $this->generateExpectedValue(
            file_get_contents(__DIR__ . '/_support/testdata-small.bin'),
            $key,
            $nonce,
            $additionalData
        );

        $this->assertEquals($nonce . $expectedValue, $encryptingStream->getContents());
    }

    /**
     * @throws \Exception
     */
    public function testCorrectlyEncryptsMediumFileWithDefaults()
    {
        $key = \sodium_crypto_aead_chacha20poly1305_keygen();

        $stream = stream_for(fopen(__DIR__ . '/_support/testdata-medium.bin', 'rb'));

        $encryptingStream = new Chacha20Poly1305Encrypt($stream, $key);

        $nonce = $encryptingStream->getNonce();

        $expectedValue = $this->generateExpectedValue(
            file_get_contents(__DIR__ . '/_support/testdata-medium.bin'),
            $key,
            $nonce
        );

        $this->assertEquals($nonce . $expectedValue, $encryptingStream->getContents());
    }

    /**
     * @throws \Exception
     */
    public function testCorrectlyEncryptsMediumFileWithAdditionalData()
    {
        $additionalData = \random_bytes(1024);

        $key = \sodium_crypto_aead_chacha20poly1305_keygen();

        $stream = stream_for(fopen(__DIR__ . '/_support/testdata-medium.bin', 'rb'));

        $encryptingStream = new Chacha20Poly1305Encrypt($stream, $key, $additionalData);

        $nonce = $encryptingStream->getNonce();

        $expectedValue = $this->generateExpectedValue(
            file_get_contents(__DIR__ . '/_support/testdata-medium.bin'),
            $key,
            $nonce,
            $additionalData
        );

        $this->assertEquals($nonce . $expectedValue, $encryptingStream->getContents());
    }

    /**
     * @throws \Exception
     */
    public function testCorrectlyEncryptsLargeFileWithDefaults()
    {
        $key = \sodium_crypto_aead_chacha20poly1305_keygen();

        $stream = stream_for(fopen(__DIR__ . '/_support/testdata-large.bin', 'rb'));

        $encryptingStream = new Chacha20Poly1305Encrypt($stream, $key);

        $nonce = $encryptingStream->getNonce();

        $expectedValue = $this->generateExpectedValue(
            file_get_contents(__DIR__ . '/_support/testdata-large.bin'),
            $key,
            $nonce
        );

        $this->assertEquals($nonce . $expectedValue, $encryptingStream->getContents());
    }

    /**
     * @throws \Exception
     */
    public function testCorrectlyEncryptsLargeFileWithAdditionalData()
    {
        $additionalData = \random_bytes(1024);

        $key = \sodium_crypto_aead_chacha20poly1305_keygen();

        $stream = stream_for(fopen(__DIR__ . '/_support/testdata-large.bin', 'rb'));

        $encryptingStream = new Chacha20Poly1305Encrypt($stream, $key, $additionalData);

        $nonce = $encryptingStream->getNonce();

        $expectedValue = $this->generateExpectedValue(
            file_get_contents(__DIR__ . '/_support/testdata-large.bin'),
            $key,
            $nonce,
            $additionalData
        );

        $this->assertEquals($nonce . $expectedValue, $encryptingStream->getContents());
    }

    /**
     * @throws \Exception
     */
    public function testCorrectlyReadsASegment()
    {
        $key = \sodium_crypto_aead_chacha20poly1305_keygen();

        $stream = stream_for(fopen(__DIR__ . '/_support/testdata-large.bin', 'rb'));

        $encryptingStream = new Chacha20Poly1305Encrypt($stream, $key);

        $nonce = $encryptingStream->getNonce();

        $expectedValue = $this->generateExpectedValue(
            file_get_contents(__DIR__ . '/_support/testdata-large.bin'),
            $key,
            $nonce
        );

        $length = random_int(1, strlen($expectedValue) - 1);

        $expectedValue = substr($nonce . $expectedValue, 0, $length);

        $this->assertEquals($expectedValue, $encryptingStream->read($length));
    }

    /**
     * @throws \Exception
     */
    public function testCorrectlySeeksEncryptedDataAndReadsRemainder()
    {
        $key = \sodium_crypto_aead_chacha20poly1305_keygen();

        $stream = stream_for(fopen(__DIR__ . '/_support/testdata-large.bin', 'rb'));

        $encryptingStream = new Chacha20Poly1305Encrypt($stream, $key);

        $nonce = $encryptingStream->getNonce();

        $expectedValue = $this->generateExpectedValue(
            file_get_contents(__DIR__ . '/_support/testdata-large.bin'),
            $key,
            $nonce
        );

        $start = random_int(1, strlen($expectedValue) - 1);
        $expectedValue = substr($nonce . $expectedValue, $start);

        $encryptingStream->seek($start);

        $this->assertEquals($expectedValue, $encryptingStream->getContents());
    }

    /**
     * @throws \Exception
     */
    public function testCorrectlySeeksEncryptedDataAndReadsASegment()
    {
        $key = \sodium_crypto_aead_chacha20poly1305_keygen();

        $stream = stream_for(fopen(__DIR__ . '/_support/testdata-large.bin', 'rb'));

        $encryptingStream = new Chacha20Poly1305Encrypt($stream, $key);

        $nonce = $encryptingStream->getNonce();

        $expectedValue = $this->generateExpectedValue(
            file_get_contents(__DIR__ . '/_support/testdata-large.bin'),
            $key,
            $nonce
        );

        $start = random_int(1, strlen($expectedValue) - 1);
        $length = random_int(1, strlen($expectedValue) - $start - 1);

        $expectedValue = substr($nonce . $expectedValue, $start, $length);

        $encryptingStream->seek($start);

        $this->assertEquals($expectedValue, $encryptingStream->read($length));
    }

    /**
     * @param string $input
     * @param string $key
     * @param string $nonce
     * @param string $additionalData
     * @return string
     */
    protected function generateExpectedValue(string $input, string $key, string $nonce, string $additionalData = ''): string
    {
        $offset = \str_repeat(\chr(0), \strlen($nonce));

        return implode('', array_map(function (string $chunk) use (&$offset, &$nonce, $key, $additionalData) {
            $result = \sodium_crypto_aead_chacha20poly1305_encrypt(
                $chunk,
                $offset . $additionalData,
                $nonce,
                $key
            );

            \sodium_increment($offset);
            \sodium_increment($nonce);

            return $result;
        }, str_split($input, 1048576)));
    }
}
