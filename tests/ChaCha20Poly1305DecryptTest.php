<?php namespace Tests;

use APPelit\Psr7\SodiumFilters\ChaCha20Poly1305Decrypt;
use Codeception\Test\Unit;
use function GuzzleHttp\Psr7\stream_for;

class ChaCha20Poly1305DecryptTest extends Unit
{

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests

    /**
     * @throws \Exception
     */
    public function testCorrectlyDecryptsWithDefaults()
    {
        $expectedValue = \random_bytes(1024);

        $key = \sodium_crypto_aead_chacha20poly1305_keygen();

        $nonce = \random_bytes(SODIUM_CRYPTO_AEAD_CHACHA20POLY1305_NPUBBYTES);

        $testString = $this->generateEncryptedValue($expectedValue, $key, $nonce);

        $stream = stream_for($nonce . $testString);

        $decryptingStream = new Chacha20Poly1305Decrypt($stream, $key);

        $this->assertEquals($expectedValue, $decryptingStream->getContents());
    }

    /**
     * @throws \Exception
     */
    public function testCorrectlyDecryptsWithAdditionalData()
    {
        $expectedValue = \random_bytes(1024);

        $additionalData = \random_bytes(1024);

        $key = \sodium_crypto_aead_chacha20poly1305_keygen();

        $nonce = \random_bytes(SODIUM_CRYPTO_AEAD_CHACHA20POLY1305_NPUBBYTES);

        $testString = $this->generateEncryptedValue($expectedValue, $key, $nonce, $additionalData);

        $stream = stream_for($nonce . $testString);

        $decryptingStream = new Chacha20Poly1305Decrypt($stream, $key, $additionalData);

        $this->assertEquals($expectedValue, $decryptingStream->getContents());
    }

    /**
     * @throws \Exception
     */
    public function testCorrectlyDecryptsSmallFileWithDefaults()
    {
        $expectedValue = file_get_contents(__DIR__ . '/_support/testdata-small.bin');

        $key = \sodium_crypto_aead_chacha20poly1305_keygen();

        $nonce = \random_bytes(SODIUM_CRYPTO_AEAD_CHACHA20POLY1305_NPUBBYTES);

        $testString = $this->generateEncryptedValue($expectedValue, $key, $nonce);

        $stream = stream_for($nonce . $testString);

        $decryptingStream = new Chacha20Poly1305Decrypt($stream, $key);

        $this->assertEquals($expectedValue, $decryptingStream->getContents());
    }

    /**
     * @throws \Exception
     */
    public function testCorrectlyDecryptsSmallFileWithAdditionalData()
    {
        $expectedValue = file_get_contents(__DIR__ . '/_support/testdata-small.bin');

        $additionalData = \random_bytes(1024);

        $key = \sodium_crypto_aead_chacha20poly1305_keygen();

        $nonce = \random_bytes(SODIUM_CRYPTO_AEAD_CHACHA20POLY1305_NPUBBYTES);

        $testString = $this->generateEncryptedValue($expectedValue, $key, $nonce, $additionalData);

        $stream = stream_for($nonce . $testString);

        $decryptingStream = new Chacha20Poly1305Decrypt($stream, $key, $additionalData);

        $this->assertEquals($expectedValue, $decryptingStream->getContents());
    }

    /**
     * @throws \Exception
     */
    public function testCorrectlyDecryptsMediumFileWithDefaults()
    {
        $expectedValue = file_get_contents(__DIR__ . '/_support/testdata-medium.bin');

        $key = \sodium_crypto_aead_chacha20poly1305_keygen();

        $nonce = \random_bytes(SODIUM_CRYPTO_AEAD_CHACHA20POLY1305_NPUBBYTES);

        $testString = $this->generateEncryptedValue($expectedValue, $key, $nonce);

        $stream = stream_for($nonce . $testString);

        $decryptingStream = new Chacha20Poly1305Decrypt($stream, $key);

        $this->assertEquals($expectedValue, $decryptingStream->getContents());
    }

    /**
     * @throws \Exception
     */
    public function testCorrectlyDecryptsMediumFileWithAdditionalData()
    {
        $expectedValue = file_get_contents(__DIR__ . '/_support/testdata-medium.bin');

        $additionalData = \random_bytes(1024);

        $key = \sodium_crypto_aead_chacha20poly1305_keygen();

        $nonce = \random_bytes(SODIUM_CRYPTO_AEAD_CHACHA20POLY1305_NPUBBYTES);

        $testString = $this->generateEncryptedValue($expectedValue, $key, $nonce, $additionalData);

        $stream = stream_for($nonce . $testString);

        $decryptingStream = new Chacha20Poly1305Decrypt($stream, $key, $additionalData);

        $this->assertEquals($expectedValue, $decryptingStream->getContents());
    }

    /**
     * @throws \Exception
     */
    public function testCorrectlyDecryptsLargeFileWithDefaults()
    {
        $expectedValue = file_get_contents(__DIR__ . '/_support/testdata-large.bin');

        $key = \sodium_crypto_aead_chacha20poly1305_keygen();

        $nonce = \random_bytes(SODIUM_CRYPTO_AEAD_CHACHA20POLY1305_NPUBBYTES);

        $testString = $this->generateEncryptedValue($expectedValue, $key, $nonce);

        $stream = stream_for($nonce . $testString);

        $decryptingStream = new Chacha20Poly1305Decrypt($stream, $key);

        $this->assertEquals($expectedValue, $decryptingStream->getContents());
    }

    /**
     * @throws \Exception
     */
    public function testCorrectlyDecryptsLargeFileWithAdditionalData()
    {
        $expectedValue = file_get_contents(__DIR__ . '/_support/testdata-large.bin');

        $additionalData = \random_bytes(1024);

        $key = \sodium_crypto_aead_chacha20poly1305_keygen();

        $nonce = \random_bytes(SODIUM_CRYPTO_AEAD_CHACHA20POLY1305_NPUBBYTES);

        $testString = $this->generateEncryptedValue($expectedValue, $key, $nonce, $additionalData);

        $stream = stream_for($nonce . $testString);

        $decryptingStream = new Chacha20Poly1305Decrypt($stream, $key, $additionalData);

        $this->assertEquals($expectedValue, $decryptingStream->getContents());
    }

    /**
     * @throws \Exception
     */
    public function testCorrectlyReadsASegment()
    {
        $expectedValue = file_get_contents(__DIR__ . '/_support/testdata-large.bin');

        $key = \sodium_crypto_aead_chacha20poly1305_keygen();

        $nonce = \random_bytes(SODIUM_CRYPTO_AEAD_CHACHA20POLY1305_NPUBBYTES);

        $testString = $this->generateEncryptedValue($expectedValue, $key, $nonce);

        $length = random_int(1, strlen($expectedValue) - 1);

        $expectedValue = substr($expectedValue, 0, $length);

        $stream = stream_for($nonce . $testString);

        $decryptingStream = new Chacha20Poly1305Decrypt($stream, $key);

        $this->assertEquals($expectedValue, $decryptingStream->read($length));
    }

    /**
     * @throws \Exception
     */
    public function testCorrectlySeeksDecryptedDataAndReadsRemainder()
    {
        $expectedValue = file_get_contents(__DIR__ . '/_support/testdata-large.bin');

        $key = \sodium_crypto_aead_chacha20poly1305_keygen();

        $nonce = \random_bytes(SODIUM_CRYPTO_AEAD_CHACHA20POLY1305_NPUBBYTES);

        $testString = $this->generateEncryptedValue($expectedValue, $key, $nonce);

        $start = random_int(1, strlen($expectedValue) - 1);

        $expectedValue = substr($expectedValue, $start);

        $stream = stream_for($nonce . $testString);

        $decryptingStream = new Chacha20Poly1305Decrypt($stream, $key);

        $decryptingStream->seek($start);

        $this->assertEquals($expectedValue, $decryptingStream->getContents());
    }

    /**
     * @throws \Exception
     */
    public function testCorrectlySeeksDecryptedDataAndReadsASegment()
    {
        $expectedValue = file_get_contents(__DIR__ . '/_support/testdata-large.bin');

        $key = \sodium_crypto_aead_chacha20poly1305_keygen();

        $nonce = \random_bytes(SODIUM_CRYPTO_AEAD_CHACHA20POLY1305_NPUBBYTES);

        $testString = $this->generateEncryptedValue($expectedValue, $key, $nonce);

        $start = random_int(1, strlen($expectedValue) - 1);
        $length = random_int(1, strlen($expectedValue) - $start);

        $expectedValue = substr($expectedValue, $start, $length);

        $stream = stream_for($nonce . $testString);

        $decryptingStream = new Chacha20Poly1305Decrypt($stream, $key);

        $decryptingStream->seek($start);

        $this->assertEquals($expectedValue, $decryptingStream->read($length));
    }

    /**
     * @param string $input
     * @param string $key
     * @param string $nonce
     * @param string $additionalData
     * @return string
     */
    protected function generateEncryptedValue(string $input, string $key, string $nonce, string $additionalData = ''): string
    {
        $offset = \str_repeat(\chr(0), \strlen($nonce));

        return implode('', array_map(function (string $chunk) use (&$offset, &$nonce, $key, $additionalData) {
            $result = \sodium_crypto_aead_chacha20poly1305_encrypt(
                $chunk,
                $offset . $additionalData,
                $nonce,
                $key
            );

            \sodium_increment($offset);
            \sodium_increment($nonce);

            return $result;
        }, str_split($input, 1048576)));
    }
}
