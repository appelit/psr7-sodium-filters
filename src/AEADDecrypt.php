<?php

namespace APPelit\Psr7\SodiumFilters;

use GuzzleHttp\Psr7\StreamDecoratorTrait;
use Psr\Http\Message\StreamInterface;

abstract class AEADDecrypt implements StreamInterface
{
    use StreamDecoratorTrait;

    protected const NONCE_LENGTH = 0;

    /** @var string */
    private $buffer = '';

    /** @var int */
    private $chunkSize;

    /** @var int */
    private $targetChunkSize;

    /** @var string */
    private $key;

    /** @var string */
    private $ad;

    /** @var string */
    private $nonce;

    /**
     * @param \Psr\Http\Message\StreamInterface $cipherText
     * @param string $key
     * @param string $ad
     * @param int $chunkSize
     */
    public function __construct(StreamInterface $cipherText, string $key, string $ad = '', int $chunkSize = 1048576)
    {
        $this->stream = $cipherText;
        $this->chunkSize = $chunkSize + SODIUM_CRYPTO_AEAD_XCHACHA20POLY1305_IETF_ABYTES;
        $this->targetChunkSize = $chunkSize;
        $this->ad = $ad;

        try {
            if (\strlen($key) !== SODIUM_CRYPTO_AEAD_XCHACHA20POLY1305_IETF_KEYBYTES) {
                throw new \InvalidArgumentException('Key is not a valid key for the algorithm');
            }

            $this->key = $key;
        } finally {
            \sodium_memzero($key);
        }
    }

    /**
     * @return string
     */
    public function getNonce(): string
    {
        if (!$this->nonce) {
            $currentPosition = $this->tell();

            $blockCount = (int)($currentPosition / $this->targetChunkSize);

            $targetPosition = $currentPosition +
                $blockCount * SODIUM_CRYPTO_AEAD_XCHACHA20POLY1305_IETF_ABYTES +
                static::NONCE_LENGTH;

            $this->stream->seek(0, SEEK_SET);

            $this->nonce = '';
            do {
                $this->nonce .= $this->stream->read(static::NONCE_LENGTH - \strlen($this->nonce));
            } while (\strlen($this->nonce) < static::NONCE_LENGTH && !$this->stream->eof());

            if (\strlen($this->nonce) < static::NONCE_LENGTH) {
                throw new \RuntimeException('Failed reading nonce');
            }

            // Set the stream to the old position + the offset caused by the nonce
            $this->stream->seek($targetPosition, SEEK_SET);
        }

        return $this->nonce;
    }

    /**
     * @inheritDoc
     */
    public function getSize()
    {
        $cipherTextSize = $this->stream->getSize();
        if ($cipherTextSize === null) {
            return null;
        }

        return max(
            0,
            $cipherTextSize -
            (int)\ceil($cipherTextSize / $this->chunkSize) * SODIUM_CRYPTO_AEAD_XCHACHA20POLY1305_IETF_ABYTES -
            static::NONCE_LENGTH
        );
    }

    /**
     * @inheritDoc
     */
    public function isWritable()
    {
        return false;
    }

    /**
     * @inheritDoc
     */
    public function read($length)
    {
        if ($length > \strlen($this->buffer)) {
            $this->buffer .= $this->decryptBlock(
                $this->chunkSize * (int)\ceil(($length - \strlen($this->buffer)) / $this->targetChunkSize)
            );
        }

        $data = \substr($this->buffer, 0, $length);
        $this->buffer = \substr($this->buffer, $length);

        return $data ? $data : '';
    }

    /**
     * @inheritDoc
     */
    public function seek($offset, $whence = SEEK_SET)
    {
        if ($whence === SEEK_CUR) {
            $offset = $this->tell() + $offset;
            $whence = SEEK_SET;
        }

        if ($whence === SEEK_SET) {
            $this->buffer = '';

            $wholeBlockOffset = (int)($offset / $this->targetChunkSize);

            $sourceWholeBlockOffset = $wholeBlockOffset * $this->chunkSize;

            $this->stream->seek($sourceWholeBlockOffset + static::NONCE_LENGTH);

            $targetWholeBlockOffset = $wholeBlockOffset * $this->targetChunkSize;

            if ($offset - $targetWholeBlockOffset > 0) {
                $this->read($offset - $targetWholeBlockOffset);
            }
        } else {
            throw new \LogicException('Unrecognized whence.');
        }
    }

    /**
     * @inheritDoc
     */
    public function tell()
    {
        $streamOffset = $this->stream->tell();
        $offsetLength = static::NONCE_LENGTH;
        if ($streamOffset < $offsetLength) {
            return 0;
        }

        $chunkCount = (int)($streamOffset / $this->chunkSize);

        return max(0, $streamOffset - $offsetLength - $chunkCount * SODIUM_CRYPTO_AEAD_XCHACHA20POLY1305_IETF_ABYTES);
    }

    /**
     * @param int $length
     * @return false|string
     */
    private function decryptBlock(int $length): string
    {
        if ($this->stream->eof()) {
            return '';
        }

        $nonce = $this->getNonce();
        $blockOffset = $this->tell() / $this->targetChunkSize;
        $offset = \str_pad(\pack('P', $blockOffset), static::NONCE_LENGTH, \chr(0), STR_PAD_RIGHT);

        // Increment the nonce with the current block offset
        \sodium_add($nonce, $offset);

        try {
            $plainText = '';
            $total = 0;

            while ($total < $length && !$this->stream->eof()) {
                try {
                    $cipherText = $this->stream->read($this->chunkSize) ?? '';
                    $read = \strlen($cipherText);

                    if ($read > 0) {
                        $result = $plainText . $this->performDecryption(
                                $cipherText,
                                $offset . $this->ad,
                                $nonce,
                                $this->key
                            );
                        \sodium_memzero($plainText);

                        $plainText = $result;
                        \sodium_memzero($result);
                    }

                    \sodium_increment($offset);
                    \sodium_increment($nonce);

                    $total += $read;
                } finally {
                    \sodium_memzero($cipherText);
                }
            }

            return $plainText;
        } finally {
            \sodium_memzero($plainText);
        }
    }

    /**
     * @param string $cipherText
     * @param string $additionalData
     * @param string $nonce
     * @param string $key
     * @return string
     */
    abstract protected function performDecryption(string $cipherText, string $additionalData, string $nonce, string $key): string;
}
