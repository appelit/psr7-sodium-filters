<?php
declare(strict_types=1);

namespace APPelit\Psr7\SodiumFilters;

class XChaCha20Poly1305Decrypt extends AEADDecrypt
{
    protected const NONCE_LENGTH = SODIUM_CRYPTO_AEAD_XCHACHA20POLY1305_IETF_NPUBBYTES;

    /**
     * @inheritDoc
     * @throws \SodiumException
     */
    protected function performDecryption(string $cipherText, string $additionalData, string $nonce, string $key): string
    {
        try {
            $result = \sodium_crypto_aead_xchacha20poly1305_ietf_decrypt($cipherText, $additionalData, $nonce, $key);

            if (!is_string($result)) {
                throw new \SodiumException('Invalid cipher text for the given key');
            }

            return $result;
        } finally {
            \sodium_memzero($cipherText);
            \sodium_memzero($key);

            if (is_string($result)) {
                \sodium_memzero($result);
            }
        }
    }
}
