<?php

namespace APPelit\Psr7\SodiumFilters;

use GuzzleHttp\Psr7\StreamDecoratorTrait;
use Psr\Http\Message\StreamInterface;

abstract class AEADEncrypt implements StreamInterface
{
    use StreamDecoratorTrait;

    protected const NONCE_BYTES = 0;

    /** @var string */
    private $buffer = '';

    /** @var int */
    private $chunkSize;

    /** @var int */
    private $targetChunkSize;

    /** @var string */
    private $key;

    /** @var string */
    private $ad;

    /** @var string */
    private $nonce;

    /** @var int */
    private $nonceOffset;

    /**
     * @param \Psr\Http\Message\StreamInterface $plainText
     * @param string $key
     * @param string $ad
     * @param int $chunkSize
     * @throws \Exception
     */
    public function __construct(StreamInterface $plainText, string $key, string $ad = '', int $chunkSize = 1048576)
    {
        $this->stream = $plainText;
        $this->chunkSize = $chunkSize;
        $this->targetChunkSize = $chunkSize + SODIUM_CRYPTO_AEAD_XCHACHA20POLY1305_IETF_ABYTES;
        $this->ad = $ad;

        try {
            if (\strlen($key) !== SODIUM_CRYPTO_AEAD_XCHACHA20POLY1305_IETF_KEYBYTES) {
                throw new \InvalidArgumentException('Key is not a valid key for the algorithm');
            }

            // Generate the nonce
            $this->nonce = \random_bytes(static::NONCE_BYTES);

            // Fill buffer with nonce
            $this->buffer = $this->nonce;

            $this->key = $key;
        } finally {
            \sodium_memzero($key);
        }
    }

    /**
     * @return string
     */
    public function getNonce(): string
    {
        return $this->nonce;
    }

    /**
     * Get the size of the stream if known.
     *
     * @return int|null Returns the size in bytes if known, or null if unknown.
     */
    public function getSize()
    {
        $plainTextSize = $this->stream->getSize();
        if ($plainTextSize === null) {
            return null;
        }

        return $plainTextSize +
            (int)\ceil($plainTextSize / $this->chunkSize) * SODIUM_CRYPTO_AEAD_XCHACHA20POLY1305_IETF_ABYTES +
            static::NONCE_BYTES;
    }

    /**
     * Returns whether or not the stream is writable.
     *
     * @return bool
     */
    public function isWritable()
    {
        return false;
    }

    /**
     * Read data from the stream.
     *
     * @param int $length Read up to $length bytes from the object and return
     *     them. Fewer than $length bytes may be returned if underlying stream
     *     call returns fewer bytes.
     * @return string Returns the data read from the stream, or an empty string
     *     if no bytes are available.
     * @throws \RuntimeException if an error occurs.
     */
    public function read($length)
    {
        if ($length > \strlen($this->buffer)) {
            $this->buffer .= $this->encryptBlock(
                $this->chunkSize * (int)\ceil(($length - \strlen($this->buffer)) / $this->targetChunkSize)
            );
        }

        $data = \substr($this->buffer, 0, $length);
        $this->buffer = \substr($this->buffer, $length);

        return $data ? $data : '';
    }

    /**
     * Seek to a position in the stream.
     *
     * @link http://www.php.net/manual/en/function.fseek.php
     * @param int $offset Stream offset
     * @param int $whence Specifies how the cursor position will be calculated
     *     based on the seek offset. Valid values are identical to the built-in
     *     PHP $whence values for `fseek()`.  SEEK_SET: Set position equal to
     *     offset bytes SEEK_CUR: Set position to current location plus offset
     *     SEEK_END: Set position to end-of-stream plus offset.
     * @throws \RuntimeException on failure.
     */
    public function seek($offset, $whence = SEEK_SET)
    {
        if ($whence === SEEK_CUR) {
            $offset = $this->tell() + $offset;
            $whence = SEEK_SET;
        }

        if ($whence === SEEK_SET) {
            $parentOffset = \max(0, $offset - static::NONCE_BYTES);

            $this->buffer = $offset < static::NONCE_BYTES ? \substr($this->nonce, $offset) : '';
            $this->nonceOffset = static::NONCE_BYTES - \strlen($this->buffer);

            $wholeBlockOffset = (int)($parentOffset / $this->targetChunkSize);

            $sourceWholeBlockOffset = $wholeBlockOffset * $this->chunkSize;

            $this->stream->seek($sourceWholeBlockOffset);

            $targetWholeBlockOffset = $wholeBlockOffset * $this->targetChunkSize;

            if ($parentOffset - $targetWholeBlockOffset > 0) {
                $this->read($parentOffset - $targetWholeBlockOffset);
            }
        } else {
            throw new \LogicException('Unrecognized whence.');
        }
    }

    /**
     * Returns the current position of the file read/write pointer
     *
     * @return int Position of the file pointer
     * @throws \RuntimeException on error.
     */
    public function tell()
    {
        $streamOffset = $this->stream->tell();
        if ($streamOffset === 0) {
            return $this->nonceOffset;
        }

        $chunkCount = (int)($streamOffset / $this->chunkSize);

        return $this->nonceOffset +
            $streamOffset +
            $chunkCount * SODIUM_CRYPTO_AEAD_XCHACHA20POLY1305_IETF_ABYTES;
    }

    /**
     * @param int $length
     * @return false|string
     */
    private function encryptBlock(int $length): string
    {
        if ($this->stream->eof()) {
            return '';
        }

        $blockOffset = (int)($this->stream->tell() / $this->chunkSize);
        $offset = \str_pad(\pack('P', $blockOffset), static::NONCE_BYTES, \chr(0), STR_PAD_RIGHT);
        $nonce = $this->nonce;

        // Increment the nonce with the current block offset
        \sodium_add($nonce, $offset);

        try {
            $cipherText = '';
            $total = 0;

            while ($total < $length && !$this->stream->eof()) {
                try {
                    $plainText = $this->stream->read($this->chunkSize) ?? '';
                    $read = \strlen($plainText);

                    if ($read > 0) {
                        $result = $cipherText . $this->performEncryption(
                                $plainText,
                                $offset . $this->ad,
                                $nonce,
                                $this->key
                            );
                        \sodium_memzero($cipherText);

                        $cipherText = $result;
                        \sodium_memzero($result);
                    }

                    \sodium_increment($offset);
                    \sodium_increment($nonce);

                    $total += $read;
                } finally {
                    \sodium_memzero($plainText);
                }
            }

            return $cipherText;
        } finally {
            \sodium_memzero($cipherText);
        }
    }

    /**
     * @param string $plainText
     * @param string $additionalData
     * @param string $nonce
     * @param string $key
     * @return string
     */
    abstract protected function performEncryption(string $plainText, string $additionalData, string $nonce, string $key): string;
}
