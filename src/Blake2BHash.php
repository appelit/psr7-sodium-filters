<?php
declare(strict_types=1);

namespace APPelit\Psr7\SodiumFilters;

use GuzzleHttp\Psr7\StreamDecoratorTrait;
use Psr\Http\Message\StreamInterface;

class Blake2BHash implements StreamInterface
{
    use StreamDecoratorTrait;

    /** @var int */
    private $chunkSize;

    /** @var string */
    private $key;

    /** @var int */
    private $hashSize;

    /** @var string */
    private $hashedValue;

    /** @var int */
    private $offset = 0;

    /**
     * @param \Psr\Http\Message\StreamInterface $plainText
     * @param int $hashSize
     * @param string $key
     * @param int $chunkSize
     */
    public function __construct(StreamInterface $plainText,
                                int $hashSize = SODIUM_CRYPTO_GENERICHASH_BYTES,
                                string $key = '',
                                int $chunkSize = 1048576)
    {
        $this->stream = $plainText;
        $this->chunkSize = $chunkSize;
        $this->hashSize = $hashSize;

        try {
            if (($length = strlen($key)) !== 0 && $length !== SODIUM_CRYPTO_GENERICHASH_KEYBYTES) {
                throw new \InvalidArgumentException('Key is not a valid key for the algorithm');
            }

            $this->key = $key;
        } finally {
            \sodium_memzero($key);
        }
    }

    /**
     * @inheritDoc
     */
    public function getSize()
    {
        return $this->hashSize;
    }

    /**
     * @inheritDoc
     */
    public function isWritable()
    {
        return false;
    }

    /**
     * @inheritDoc
     */
    public function read($length)
    {
        if ($this->offset > $this->hashSize) {
            return '';
        }

        if (!$this->hashedValue) {
            $this->generateHash();
        }

        $result = substr($this->hashedValue, $this->offset, $length);
        $this->offset += strlen($result);

        return $result;
    }

    /**
     * @inheritDoc
     */
    public function eof()
    {
        return $this->offset >= $this->hashSize;
    }

    /**
     * @inheritDoc
     */
    public function seek($offset, $whence = SEEK_SET)
    {
        if ($whence === SEEK_CUR) {
            $offset = $this->tell() + $offset;
            $whence = SEEK_SET;
        }

        if ($whence === SEEK_SET) {
            $this->offset = $offset;
        } else {
            throw new \LogicException('Unrecognized whence.');
        }
    }

    private function generateHash(): void
    {
        $currentPosition = $this->stream->tell();

        $this->stream->seek(0);

        $state = sodium_crypto_generichash_init($this->key, $this->hashSize);

        while (!$this->stream->eof()) {
            $read = $this->stream->read($this->chunkSize);

            sodium_crypto_generichash_update($state, $read);
        }

        $this->hashedValue = sodium_crypto_generichash_final($state, $this->hashSize);

        $this->stream->seek($currentPosition, SEEK_SET);
    }
}
