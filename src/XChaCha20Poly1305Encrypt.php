<?php
declare(strict_types=1);

namespace APPelit\Psr7\SodiumFilters;

class XChaCha20Poly1305Encrypt extends AEADEncrypt
{
    protected const NONCE_BYTES = SODIUM_CRYPTO_AEAD_XCHACHA20POLY1305_IETF_NPUBBYTES;

    /**
     * @inheritDoc
     */
    protected function performEncryption(string $plainText, string $additionalData, string $nonce, string $key): string
    {
        try {
            return \sodium_crypto_aead_xchacha20poly1305_ietf_encrypt($plainText, $additionalData, $nonce, $key);
        } finally {
            \sodium_memzero($plainText);
            \sodium_memzero($key);
        }
    }
}
